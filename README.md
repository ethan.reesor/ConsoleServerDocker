# ConsoleServerDocker
### Docker Extention for ConsoleServerDocker
![ConsoleServerDocker NuGet Version](https://img.shields.io/nuget/v/ConsoleServerDocker.svg?style=flat&label=NuGet%3A%20ConsoleServerDocker)


Provides helper classes for Docker combined with ![ConsoleServer](https://gitlab.com/logicethos/consoleserver)
Allows you to create console apps that interact with docker containers.
