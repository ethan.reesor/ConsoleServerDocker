using ConsoleServer;
using Docker.DotNet.Models;
using Spectre.Console;

namespace ConsoleServerDocker;

public class clsProgressOutput : IProgress<JSONMessage>
{
    private AnsiTelnetConsole AnsiConsole;
    private string LastStatus = String.Empty;
    public bool DownloadStarted { get; private set; }

    public clsProgressOutput(AnsiTelnetConsole console)
    {
        AnsiConsole = console;
    }

    public void Report(JSONMessage value)
    {
        if (String.IsNullOrEmpty(value?.Status)) return;

        if (DownloadStarted)
        {
            AnsiConsole.Write($"\r{value.Status} {value.ProgressMessage}");    
        }
        else if (value.ProgressMessage != null)
        {
            AnsiConsole.Write($"\r{value.Status} {value.ProgressMessage}");
            DownloadStarted = true;
        }
        else if (LastStatus != value.Status)
        {
            AnsiConsole.WriteLine($"{value.Status} {value.ErrorMessage}");
            LastStatus = value.Status;
        }

    }
}
