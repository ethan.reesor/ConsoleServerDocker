using ConsoleServer;
using Docker.DotNet;
using Docker.DotNet.Models;
using Serilog;
using ILogger = Serilog.ILogger;

namespace ConsoleServerDocker;

public class clsDockerImageNano : IDockerImage
{
    private AnsiTelnetConsole ANSIConsole;
    
    ImagesCreateParameters AlpineImage = new ImagesCreateParameters
    {
        FromImage = "alpine:3.16",
        Repo = "alpine",
        Tag = "3.16",
    };

    public clsDockerImageNano(AnsiTelnetConsole console)
    {
        ANSIConsole = console;
    }

    public CreateContainerResponse Create(string containerName ,string volume, string filepath)
    {

        IProgress<JSONMessage> progress = (ANSIConsole != null) ? new clsProgressOutput(ANSIConsole) : new Progress();

        //Pull Image

        clsDockerManager.Instance.CreateImageAsync(AlpineImage, new AuthConfig(), progress).Wait();
            
        //Set container Run Parameters
        var runParams = new CreateContainerParameters
        {
            Name = containerName,
            Env = null,
            Cmd = null,
            ArgsEscaped = false,
            Image = AlpineImage.FromImage,
            Entrypoint = new string[] {"sh"},
            Labels = null,
            AttachStderr = true,
            AttachStdin = true,
            AttachStdout = true,
            OpenStdin = true,
            StdinOnce = true,
            Tty = true,
            HostConfig = new HostConfig
            {
                Binds = new List<string>()
                {
                    $"{volume}:/data",
                },
                Privileged = true,
                AutoRemove = true,
                PidMode = "host",
                CgroupnsMode = "host",
                ReadonlyRootfs = false,
            }
        };

        clsDockerManager.Instance.DeleteContainerIfExist(runParams.Name).Wait();

        //Create container
        var result =  clsDockerManager.Instance.CreateContainerAsync(runParams).Result;
        
        Log.Logger.Information("{clsDockerImageNsenter}: {result}",nameof(clsDockerImageNano),result);
        foreach (var warn in result.Warnings)
        {
            Log.Logger.Warning("{image} {warn}",AlpineImage.Repo,warn);
        }
        
        return result;
    }
    
}