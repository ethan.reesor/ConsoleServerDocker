using System.Net.Http.Json;
using System.Linq;
using Serilog;

namespace ConsoleServerDocker;

public class clsGitLab
{
    private UInt32 ProjectID { get; init; }
    private UInt32 RepoID { get; init; }
    
    public clsGitLab(UInt32 projectID, UInt32 repoId)
    {
        ProjectID = projectID;
        RepoID = repoId;
    }
    
    /*public clsGitLab(string projectPath)
    {
        var registry = GetDockerRegistory(projectPath).Result;
        ProjectID = registry.id;
    }*/
    
    /// <summary>
    /// gets
    /// </summary>
    /// <returns></returns>
    public async Task<List<GitLabRegistry>> GetDockerRegistory()
    {
        using (var httpClient = new HttpClient())
        {
            return  await httpClient.GetFromJsonAsync<List<GitLabRegistry>>(
                $"https://gitlab.com/api/v4/projects/{ProjectID}/registry/repositories");
        }
    }

    public async Task<List<GitLabImages>> GetDockerImages(UInt32 dockerRegistoryID = 0)
    {
        if (dockerRegistoryID == 0) dockerRegistoryID = RepoID; 
        //https://gitlab.com/api/v4/projects/29762666/registry/repositories/2858961/tags/v1-0-0-rc1
        
        var url = $"https://gitlab.com/api/v4/projects/{ProjectID}/registry/repositories/{dockerRegistoryID}/tags";
        
        using (var httpClient = new HttpClient())
        {
            var images = await httpClient.GetFromJsonAsync<List<GitLabImages>>(
                $"https://gitlab.com/api/v4/projects/{ProjectID}/registry/repositories/{dockerRegistoryID}/tags");
            
            return images;
        }
    }
    
    public async Task<Commits> GetCommit(string tag)
    {
        using (var httpClient = new HttpClient())
        {
            var images = await httpClient.GetFromJsonAsync<Commits>(
                $"https://gitlab.com/api/v4/projects/{ProjectID}/repository/tags/{tag}");
            
            return images;
        }
    }

    public async Task<List<Commits>> GetCommits()
    {
        using (var httpClient = new HttpClient())
        {
            var images = await httpClient.GetFromJsonAsync<List<Commits>>(
                $"https://gitlab.com/api/v4/projects/{ProjectID}/repository/tags");

            return images;
        }
    }

    public async Task<List<GitLabImages>> ListTags2()
    {
        try
        {
            using (var httpClient = new HttpClient())
            {
                var repositories = await httpClient.GetFromJsonAsync<List<GitLabRegistry>>(
                    $"https://gitlab.com/api/v4/projects/{ProjectID}/registry/repositories");

                var images = await httpClient.GetFromJsonAsync<List<GitLabImages>>(
                    $"https://gitlab.com/api/v4/projects/{ProjectID}/registry/repositories/{RepoID}/tags");

                return images;
            }
        }
        catch (HttpRequestException) // Non success
        {
            Log.Logger.Error("{gitlab}: An error occurred.",nameof(clsGitLab));
        }
        catch (NotSupportedException) // When content type is not valid
        {
            Log.Logger.Error("{gitlab}: The content type is not supported.",nameof(clsGitLab));
        }

        return null;
    }
}


//https://gitlab.com/api/v4/projects/{RepoID}/registry/repositories/app/tags
public class GitLabImages
{
    public string name { get; set; }
    public string path { get; set; }
    public string location { get; set; }
}

//"https://gitlab.com/api/v4/projects/{ProJectID}/registry/repositories"
public class GitLabRegistry
{
    public UInt32 id { get; set; }
    public string name { get; set; }
    public string path { get; set; }
    public int project_id { get; set; }
    public string location { get; set; }
    public DateTime created_at { get; set; }
    public DateTime cleanup_policy_started_at { get; set; }
}


public class Commit
{
    public string id { get; set; }
    public string short_id { get; set; }
    public DateTime created_at { get; set; }
    public List<string> parent_ids { get; set; }
    public string title { get; set; }
    public string message { get; set; }
    public string author_name { get; set; }
    public string author_email { get; set; }
    public DateTime authored_date { get; set; }
    public string committer_name { get; set; }
    public string committer_email { get; set; }
    public DateTime committed_date { get; set; }
  //  public Trailers trailers { get; set; }
    public string web_url { get; set; }
}

//https://gitlab.com/api/v4/projects/{ProjectID}/repository/tags/{tag}
public class Commits
{
    public string name { get; set; }
    public string message { get; set; }
    public string target { get; set; }
    public Commit commit { get; set; }
    public object release { get; set; }
    public bool @protected { get; set; }
}