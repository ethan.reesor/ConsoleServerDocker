using System.IO.Pipelines;
using System.Text;
using Docker.DotNet.Models;
using ICSharpCode.SharpZipLib.Tar;

namespace ConsoleServerDocker;

public class clsWriteFilesToContainer : IDisposable
{
    private Pipe outputPipe;
    private TarOutputStream tarWrite;
    public string ContainerID { get; set; }
    private string OutputPath;
    private TarHeader header;

    public clsWriteFilesToContainer(string containerID, string outputPath)
    {
        OutputPath = outputPath;
        ContainerID = containerID;
        outputPipe = new Pipe();
        tarWrite = new ICSharpCode.SharpZipLib.Tar.TarOutputStream(outputPipe.Writer.AsStream());
        header = new TarHeader();
    }

    public void WriteFile(string inputFilePath, string outputFilename = null)
    {
        var fileName = Path.GetFileName(inputFilePath);
        if (String.IsNullOrEmpty(outputFilename)) outputFilename = fileName;

        using (var stream = File.Open(inputFilePath, FileMode.Open))
        {
            WriteFile(stream, outputFilename);
        }
    }

    public void WriteFile(Stream stream, string outputFilename, TarEntry tarEntry = null)
    {
        if (tarEntry == null)
        {
            tarEntry = new TarEntry(header)
            {
                Name = outputFilename,
                // UserId = 0,
                // GroupId = 0,
                // UserName = null,
                // GroupName = null,
                ModTime = DateTime.UtcNow,
            };
        }

        tarEntry.Size = stream.Length;

        tarWrite.PutNextEntry(tarEntry);
        stream.Position = 0;
        stream.CopyToAsync(tarWrite);
    }


    public void Dispose()
    {
        tarWrite.CloseEntry();
        tarWrite.Dispose();

        var containerPathStatParameters = new ContainerPathStatParameters()
        {
            Path = OutputPath
        };

        clsDockerManager.Instance.GetClient().Containers.ExtractArchiveToContainerAsync(ContainerID,containerPathStatParameters, outputPipe.Reader.AsStream()).Wait();

        outputPipe.Writer.AsStream().Dispose();
        outputPipe.Reader.AsStream().Dispose();
    }

}